﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonController : MonoBehaviour
{

    public GameObject Target;
    public float SmoothSpeed = 0.25f;
    public Vector2 OffsetPosition; /*TBD: camera offset start position */

    void LateUpdate()
    {
        var t = new Vector2(Target.transform.position.x, transform.position.y);
        var tmp_target = t + OffsetPosition;

        var smoothPos = Vector2.Lerp(transform.position, tmp_target, SmoothSpeed * Time.deltaTime);
        transform.position = new Vector3(smoothPos.x, smoothPos.y, transform.position.z);
    }
}
