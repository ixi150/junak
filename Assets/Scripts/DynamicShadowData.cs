﻿using UnityEngine;

[CreateAssetMenu]
public class DynamicShadowData : ScriptableDataObject<DynamicShadowData>
{
    [SerializeField]
    GameObject playerShadowPrefab = null, 
        cityShadowPrefab = null;

    [Range(0.5f, 2.0f)]
    [SerializeField]
    float scale = 1;

    [SerializeField]
    Vector2 offset = Vector2.zero;

    public static GameObject CreateShadow(GameObject original, DynamicShadow.ShadowType shadowType)
    {
        var shadow = Instantiate(Ref.GetPrefab(shadowType), original.transform);
        shadow.name = original.name + "'s shadow";
        var spriteRenderer = original.GetComponent<SpriteRenderer>();
        var shadowMask = shadow.GetComponent<SpriteMask>();
        if (spriteRenderer && shadowMask)
        {
            shadowMask.sprite = spriteRenderer.sprite;
        }
        return shadow;
    }

    public static Vector3 GetOffset()
    {
        return Ref.offset;
    }

    public static Vector3 GetScale()
    {
        return Ref.scale * Vector3.one;
    }

    GameObject GetPrefab(DynamicShadow.ShadowType shadowType)
    {
        switch (shadowType)
        {
            case DynamicShadow.ShadowType.PlayerShadow: return playerShadowPrefab;
            case DynamicShadow.ShadowType.CityShadow: return cityShadowPrefab;
            default:return null;
        }
    }
}
