﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : GameBehaviour
{
    public static event Action MainMenuEntered;
    public static event Action NewGameStarted;
    public static event Action InstructionsOpened;
    public static event Action InstructionsClosed;
    public static event Action GameEnded;

    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Button tutorialButton;

    [SerializeField]
    private Image tutorialPartOne;
    [SerializeField]
    private Image tutorialPartTwo;
    [SerializeField]
    private Button tutorialButtonNext;
    [SerializeField]
    private Button tutorialButtonPrevious;
    [SerializeField]
    private Button tutorialButtonClose;

    [SerializeField]
    private GameObject oilMeter;

    [SerializeField]
    private CanvasGroup deathPanel;

    [SerializeField]
    private Text distanceText;

    [SerializeField]
    private Button repeatButton;

    [SerializeField]
    private Button mainMenuButton;

    [SerializeField]
    private Image fadeOutImage;

    private static MainMenuManager mainMenuManager;

    private bool shouldRestart;
    private bool shouldLoadMainMenu;

    private void Awake()
    {
        mainMenuManager = this;
    }

	// Use this for initialization
	void Start ()
    {
        tutorialButton.onClick.AddListener(OnTutorialButtonPressed);
        tutorialButtonPrevious.onClick.AddListener(OnTutorialPreviousButtonPressed);
        tutorialButtonClose.onClick.AddListener(OnTutorialCloseButtonPressed);
        tutorialButtonNext.onClick.AddListener(OnTutorialNextButtonPressed);
        startButton.onClick.AddListener(OnPlayButtonPressed);
        repeatButton.onClick.AddListener(OnRepeatButtonPressed);
        mainMenuButton.onClick.AddListener(OnRestartButtonPressed);

        deathPanel.gameObject.SetActive(false);
        deathPanel.alpha = 0;
        repeatButton.interactable = false;
        mainMenuButton.interactable = false;
        tutorialPartOne.gameObject.SetActive(false);
        tutorialPartTwo.gameObject.SetActive(false);
        tutorialButtonPrevious.interactable = false;
        tutorialButtonNext.interactable = false;
        tutorialButtonClose.interactable = false;

        RaiseEvent(MainMenuEntered);
    }

    public static MainMenuManager GetMainMenuManager()
    {
        return mainMenuManager;
    }

    public void EnablePlayMode()
    {
        oilMeter.gameObject.SetActive(true);
    }

    private void OnPlayButtonPressed()
    {
        MotoController.GetMotoController().PlayGame();
        tutorialPartOne.gameObject.SetActive(false);
        tutorialPartTwo.gameObject.SetActive(false);
        tutorialButtonPrevious.interactable = false;
        tutorialButtonNext.interactable = false;
        tutorialButtonClose.interactable = false;
        startButton.gameObject.SetActive(false);
        tutorialButton.gameObject.SetActive(false);

        RaiseEvent(NewGameStarted);
    }

    private void OnTutorialButtonPressed()
    {
        startButton.interactable = false;
        tutorialButton.interactable = false;
        tutorialPartOne.gameObject.SetActive(true);
        tutorialButtonNext.interactable = true;

        RaiseEvent(InstructionsOpened);
    }

    private void OnTutorialNextButtonPressed()
    {
        tutorialPartOne.gameObject.SetActive(false);
        tutorialPartTwo.gameObject.SetActive(true);
        tutorialButtonNext.interactable = false;
        tutorialButtonPrevious.interactable = true;
        tutorialButtonClose.interactable = true;
    }

    private void OnTutorialPreviousButtonPressed()
    {
        tutorialPartOne.gameObject.SetActive(true);
        tutorialPartTwo.gameObject.SetActive(false);
        tutorialButtonPrevious.interactable = false;
        tutorialButtonClose.interactable = false;
        tutorialButtonNext.interactable = true;
    }

	private void OnTutorialCloseButtonPressed()
	{
        startButton.interactable = true;
        tutorialButton.interactable = true;

        tutorialPartOne.gameObject.SetActive(false);
        tutorialPartTwo.gameObject.SetActive(false);
        tutorialButtonPrevious.interactable = false;
        tutorialButtonClose.interactable = false;
        tutorialButtonNext.interactable = false;

        RaiseEvent(InstructionsClosed);
    }

    public void ShowDeathScreen()
    {
        StartCoroutine(DeathScreenSeq());

        RaiseEvent(GameEnded);
    }

    private IEnumerator DeathScreenSeq()
    {
        repeatButton.interactable = true;
        mainMenuButton.interactable = true;
        fadeOutImage.color = new Color(fadeOutImage.color.r, fadeOutImage.color.g, fadeOutImage.color.b, 0);
        distanceText.text = ((int)(MotoController.GetMotoController().currentDistance)).ToString() + "m";
        deathPanel.gameObject.SetActive(true);

        while(deathPanel.alpha < 1f)
        {
            deathPanel.alpha = deathPanel.alpha + 10f * Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }

    private void OnRepeatButtonPressed()
    {
        shouldRestart = true;
        StartCoroutine(FadeOut());

        RaiseEvent(NewGameStarted);
    }

    private void OnRestartButtonPressed()
    {
        shouldLoadMainMenu = true;
        StartCoroutine(FadeOut());

        RaiseEvent(MainMenuEntered);
    }

    private IEnumerator FadeOut()
    {
        while(fadeOutImage.color.a < 1f)
        {
            fadeOutImage.color = new Color(fadeOutImage.color.r, fadeOutImage.color.g, fadeOutImage.color.b, fadeOutImage.color.a + 0.01f);
            yield return new WaitForSeconds(0.01f);
        }

        if(shouldLoadMainMenu)
        {
            Application.LoadLevel(0);
        }
        else if(shouldRestart)
        {
            TerrainGenerator.GetTerrainGenerator().GenerateSegments = false;
            TerrainGenerator.GetTerrainGenerator().Reset();

            MotoController.GetMotoController().Reset();
            TerrainGenerator.GetTerrainGenerator().GenerateSegments = true;
            MotoController.GetMotoController().EnableAfterReset();

            deathPanel.alpha = 0f;
            deathPanel.gameObject.SetActive(false);
        }
    }
}
