﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameBehaviour : MonoBehaviour
{
    protected void RaiseEvent(Action action)
    {
        if (action == null) return;
        action();
    }
}
