﻿using UnityEngine;

public class ScriptableDataObject<T> : ScriptableObject where T : ScriptableDataObject<T>
{
    protected static T Ref
    {
        get
        {
            if (_ref == null)
            {
                var type = typeof(T);
                _ref = Resources.Load(type.ToString(), type) as T;
            }
            return _ref;
        }
    }

    static T _ref;
}
