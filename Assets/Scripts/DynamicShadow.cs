﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DynamicShadow : MonoBehaviour
{
    public enum ShadowType
    {
        PlayerShadow =0,
        CityShadow=1,
    }

    [SerializeField]
    ShadowType shadowType = ShadowType.PlayerShadow;

    Transform _shadowTransform;

    void Awake()
    {
        _shadowTransform = DynamicShadowData.CreateShadow(gameObject, shadowType).transform;
    }

    void LateUpdate()
    {
        UpdateShadowProsperties();
    }

    void UpdateShadowProsperties()
    {
        _shadowTransform.localScale = DynamicShadowData.GetScale();
        _shadowTransform.position = transform.position + DynamicShadowData.GetOffset();
    }
}
