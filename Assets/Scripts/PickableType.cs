﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickableType
{
    None = 0,
    Pasztecik = 1,
    Oil = 2,
    Paprykarz = 3
}
