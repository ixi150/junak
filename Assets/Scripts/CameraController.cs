﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public bool isRunning;

    public GameObject Target;
    public float SmoothSpeed = 0.25f;
    [Tooltip("Not implemented")]
    public Vector2 OffsetPosition; /*TBD: camera offset start position */
    public float RotationSpeed = 100;
    public float RotationLerp = 10;

    void LateUpdate()
    {
        if (!isRunning) return;
        var t = new Vector2(Target.transform.position.x, Target.transform.position.y);
        var tmp_target = t + OffsetPosition;

        var smoothPos = Vector2.Lerp(transform.position, tmp_target, SmoothSpeed * Time.deltaTime);
        transform.position = new Vector3(smoothPos.x, smoothPos.y, transform.position.z);
        Tilt();
    }

    private void Tilt()
    {
        /*
			- if on ground -> rotate/tilt
		*/
        var target = Quaternion.Euler(0, 0, Input.acceleration.x * RotationSpeed);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * RotationLerp);
    }
}
