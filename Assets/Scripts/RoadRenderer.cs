﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoadRenderer : MonoBehaviour
{
    public EdgeCollider2D edgeCollider = null;

    [SerializeField]
    Transform bigDownFill = null, littleFillsParent = null;

    private void Update()
    {
        if (edgeCollider == null) return;
        if (bigDownFill == null) return;
        if (littleFillsParent == null) return;

        var points = edgeCollider.points;
        var allX = points.Select(_ => _.x);
        var xMin = allX.Min();
        var xMax = allX.Max();
        var allY = points.Select(_ => _.y);
        var yMin = allY.Min();
        var yMax = allY.Max();
        var rect = new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
        transform.position = edgeCollider.transform.position + (Vector3)rect.position;
        transform.localScale = Vector3.one;
        bigDownFill.localScale = new Vector3(rect.width, -100, 1);

        if (littleFillsParent.childCount > points.Length)
        {
            Destroy(littleFillsParent.GetChild(littleFillsParent.childCount - 1).gameObject);
        }

        for (int i = 0; i < points.Length - 1; i++)
        {
            SetLittleFill(points[i], points[i + 1], i, -rect.height);
        }

        if (Application.isPlaying)
        {
            enabled = false;
        }
    }

    private void SetLittleFill(Vector2 begin, Vector2 end, int childIndex, float height)
    {
        while (littleFillsParent.childCount < childIndex + 1)
        {
            Instantiate(littleFillsParent.GetChild(0), littleFillsParent);
        }

        var t = littleFillsParent.GetChild(childIndex);
        t.position = (Vector3)begin + edgeCollider.transform.position;
        t.eulerAngles= new Vector3(0,0, Vector2.SignedAngle(Vector2.right, end - begin));
        t.localScale = new Vector3((end - begin).magnitude, height, 1);
    }
}
