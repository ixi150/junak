﻿using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public GameObject MapContainer;

    [HideInInspector]
    public GameObject Player;

    public List<GameObject> RoadSegmentPrefabs;
    public List<GameObject> PickablePrefabs;
    public List<GameObject> BuildingPrefabs;

    private void Start()
    {
        if (Player==null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }
    }
}