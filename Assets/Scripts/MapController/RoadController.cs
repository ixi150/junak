﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadController : MonoBehaviour
{
    private MapController MapController;

    private float distanceToRoadAhead = float.MaxValue;
    private float distanceToRoadBehind = float.MinValue;

    private const int MinDistanceToRoadAhead = 250;
    private const int MaxDistanceToRoadBehind = 250;

    public int buildingChance = 10;

    public int MaxCooldown = 5;

    private List<GameObject> Road;

    private void Awake()
    {
        MapController = GetComponentInParent<MapController>();
        Road = new List<GameObject>();
    }

    private void Start()
    {
        var roadStart = Instantiate(MapController.RoadSegmentPrefabs[0], Vector3.zero, Quaternion.identity, MapController.MapContainer.transform);
        GenerateBuildingsOnRoadNodes(roadStart);
        Road.Add(roadStart);

        var initialRoadSegmentOffset = Vector3.right * GetSegmentWidth(roadStart);
        var randomSegmentPrefab = MapController.RoadSegmentPrefabs[Random.Range(0, MapController.RoadSegmentPrefabs.Count)];

        var roadEnd = Instantiate(randomSegmentPrefab, roadStart.transform.position + initialRoadSegmentOffset, Quaternion.identity, MapController.MapContainer.transform);
        GenerateBuildingsOnRoadNodes(roadEnd);
        Road.Add(roadEnd);
    }

    private void Update()
    {
        distanceToRoadAhead = Mathf.Abs(Road.Last().transform.position.x - MapController.Player.transform.position.x);

        distanceToRoadBehind = Mathf.Abs(MapController.Player.transform.position.x - Road.First().transform.position.x);

        if (distanceToRoadAhead < MinDistanceToRoadAhead)
        {
            GenerateNewRoadSegment(Road.Last().transform.position);
        }
        if (distanceToRoadBehind > MaxDistanceToRoadBehind)
        {
            Destroy(Road.FirstOrDefault());
            Road.RemoveAt(0);
        }
    }

    private void GenerateNewRoadSegment(Vector3 position)
    {
        var previousRoadSegmentSize = new Vector3(GetSegmentWidth(Road.Last()), 0);
        var road = Instantiate(MapController.RoadSegmentPrefabs[Random.Range(0, MapController.RoadSegmentPrefabs.Count)], position + previousRoadSegmentSize, Quaternion.identity, MapController.MapContainer.transform);
        GenerateBuildingsOnRoadNodes(road);
        Road.Add(road);
    }

    private float GetSegmentWidth(GameObject gameObject)
    {
        var edgeCollider2d = gameObject.GetComponentInChildren<EdgeCollider2D>();

        var edgeBeginning = edgeCollider2d.points.Select(p => p.x).Min();
        var edgeEnd = edgeCollider2d.points.Select(p => p.x).Max();

        var distance = Mathf.Abs(edgeBeginning - edgeEnd);

        return distance;
    }

    private void GenerateBuildingsOnRoadNodes(GameObject road)
    {
        var roadSegment = road.GetComponentInChildren<RoadSegment>();
        var cooldown = 0;

        foreach(var point in roadSegment.EdgeCollider2D.points)
        {
            var x = Random.Range(0, 101);
            if (x < buildingChance && cooldown-- <= 0)
            {
                var building = Instantiate(MapController.BuildingPrefabs[Random.Range(0, MapController.BuildingPrefabs.Count)], road.transform.position + (Vector3)point, Quaternion.identity, road.transform);
                building.transform.position += new Vector3(0,building.GetComponent<SpriteRenderer>().bounds.size.y/4,0);
                roadSegment.Buildings.Add(building);
                cooldown = MaxCooldown;
            }
        }
    }
}