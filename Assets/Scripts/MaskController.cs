﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskController : MonoBehaviour {

	public Camera Camera;
	public float OffsetX;
	public float StartPosX;
	private SpriteRenderer _mySprite;

	private void Start()
	{
		_mySprite = GetComponent<SpriteRenderer>();
	}

	private void Update ()
	{
		// mask follow camera with offset in X
		_mySprite.size = new Vector2 ( (Camera.transform.position.x * 2) + OffsetX + StartPosX*100, _mySprite.size.y);
		transform.position = new Vector2(Camera.transform.position.x - StartPosX, transform.position.y);
	}
}
