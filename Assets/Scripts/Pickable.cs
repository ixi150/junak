﻿using UnityEngine;

public class Pickable : MonoBehaviour
{
    public PickableType pickableType;
    public float boostValue;

    public void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (collider2D == null) return;

        var controller = collider2D.GetComponent<MotoController>();

        if (controller == null) return;

        controller.CollectPickable(this);

        Destroy(this.gameObject);
    }
}