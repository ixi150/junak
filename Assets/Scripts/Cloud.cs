﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour 
{

    [SerializeField]
    private float cloudMoveSpeed;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.x < MotoController.GetMotoController().playerGameObject.transform.position.x && Mathf.Abs(MotoController.GetMotoController().playerGameObject.transform.position.x - transform.position.x) >= 150)
        {
            Destroy(this.gameObject);
        }
	}

	private void LateUpdate()
	{
        this.transform.position = new Vector3(transform.position.x - cloudMoveSpeed * Time.deltaTime, transform.position.y, transform.position.z);
	}
}
