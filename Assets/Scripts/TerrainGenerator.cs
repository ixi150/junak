﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {

	public Segment StartSegment;
	public Segment[] AvaliableSegments;

	public Object[] Pickables;
	public float PickableChance;

	public Object[] Obstacles;
	public float ObstacleChance;

	public bool GenerateSegments;
	private List<Segment> _segments;
	private int segs = 100;

    private List<GameObject> clouds = new List<GameObject>();
    [SerializeField]
    private GameObject[] cloudsPrefabs;

    private int cloudsMaxCount = 25;
    [SerializeField]
    private float distBetweenClouds;
    [SerializeField]
    private float cloudHeightDifference;
    [SerializeField]
    private float cloudMinHeight;

    private Transform lastCloudTrans;

    public static TerrainGenerator GetTerrainGenerator()
    {
        return terrainGenerator;
    }

    private static TerrainGenerator terrainGenerator;

	private void Awake()
	{
        terrainGenerator = this;
	}

	private void Start ()
	{
		_segments = new List<Segment>();
		_segments.Add(StartSegment);
	}

	void Update ()
	{
		if (!GenerateSegments) return;
		while (segs-- > 0)
			GenerateNextSegment(_segments[_segments.Count - 1]);

        GenerateClouds();
	}

    private void GenerateClouds()
    {
        if (clouds.Count < cloudsMaxCount)
        {
            GameObject newCloud = Instantiate(cloudsPrefabs[Random.Range(0, cloudsPrefabs.Length)]);
            var newCloudTrans = newCloud.transform;
            newCloudTrans.SetParent(this.transform, false);
            newCloudTrans.position = new Vector3((lastCloudTrans != null) ? lastCloudTrans.position.x + Random.Range(distBetweenClouds / 2, distBetweenClouds) : 0f, cloudMinHeight + Random.Range(0, cloudHeightDifference));
            clouds.Add(newCloud);
            lastCloudTrans = newCloudTrans;
        }

        for (int i = 0; i < clouds.Count; i++)
        {
            if (clouds[i] == null)
            {
                clouds.RemoveAt(i);
            }
        }
    }

	private void GenerateNextSegment(Segment lastSegment)
	{
		Segment newSegment = GetRandomSegment(lastSegment);
		newSegment.obstacles = Obstacles;
		newSegment.obstacleChance = ObstacleChance;
		newSegment.pickables = Pickables;
		newSegment.pickableChance = PickableChance;
	
		var anch_y = newSegment.StartAnchorY;
		if (newSegment.SegmentType == SegmentType.JUMP_END)
		{
			anch_y += Random.Range(0, 6);
		}
		Vector2 pos = new Vector2(lastSegment.transform.position.x + newSegment.SegWidth*2, lastSegment.transform.position.y + (lastSegment.EndAnchorY - anch_y));
		var x = Instantiate(newSegment, pos, Quaternion.identity);
		x.transform.parent = this.transform;
		_segments.Add(x);
	}

	private Segment GetRandomSegment(Segment lastSegment)
	{
		var rnd = Mathf.Floor(Random.Range(0, 100));
		var t = 100;
		while(t --> 0)
		{
			var newSegment = AvaliableSegments[Random.Range(0, AvaliableSegments.Length)];
			var lst = lastSegment.SegmentType;
			var nwt = newSegment.SegmentType;

			if (lst == SegmentType.HILL)
			{
				if (newSegment.SegmentType == SegmentType.JUMP_END)
					continue;
			}
			else if (lst == SegmentType.JUMP_START)
			{
				if (newSegment.SegmentType != SegmentType.JUMP_END)
					continue;
			}
			else if (lst == SegmentType.JUMP_END)
			{
				if (newSegment.SegmentType == SegmentType.JUMP_END)
					continue;
			}
			return newSegment;
		}
		return null;
	}

	public void Reset()
	{
        for (int i = 0; i < _segments.Count; i++)
        {
            if (_segments[i] != null)
            {
                _segments[i].GetComponent<Segment>().ClearAll();
                Destroy(_segments[i].gameObject);
            }
        }

        _segments.Add(StartSegment);
        _segments.Clear();

        for (int i = 0; i < clouds.Count; i++)
        {
            Destroy(clouds[i]);
        }
	}
}
