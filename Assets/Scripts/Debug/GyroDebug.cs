﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class GyroDebug : MonoBehaviour
{
    Text text;

    private void Awake()
    {
        text = GetComponent<Text>();

    }

    private void Update()
    {
        text.text = Input.acceleration.ToString();
    }
}
