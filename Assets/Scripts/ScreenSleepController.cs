﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class ScreenSleepController
{
    static bool isInitialized = false;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Initialize()
    {
        if (isInitialized) return;
        MainMenuManager.MainMenuEntered += OnMainMenuEnter;
        MainMenuManager.NewGameStarted += OnNewGameStart;
        MainMenuManager.GameEnded += OnGameEnd;
        isInitialized = true;
    }

    static void OnMainMenuEnter()
    {
        AllowScreenSleep();
    }

    static void OnNewGameStart()
    {
        DisableScreenSleep();
    }

    static void OnGameEnd()
    {
        AllowScreenSleep();
    }

    static void AllowScreenSleep()
    {
        Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }

    static void DisableScreenSleep()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}
