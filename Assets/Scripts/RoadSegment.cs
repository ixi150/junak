﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoadSegment : MonoBehaviour
{
    public EdgeCollider2D EdgeCollider2D;
    public List<GameObject> Buildings;
    private LineRenderer LineRenderer;

    [SerializeField]
    private GameObject RoadRendererPrefab;

    private void Awake()
    {
        EdgeCollider2D = GetComponent<EdgeCollider2D>();
        LineRenderer = GetComponent<LineRenderer>();

        if (Application.isPlaying)
        {
            var roadRenderer = Instantiate(RoadRendererPrefab, transform).GetComponent<RoadRenderer>();
            roadRenderer.edgeCollider = EdgeCollider2D;
        }
    }

    private void Update()
    {
        if (EdgeCollider2D == null || LineRenderer == null) return;

        LineRenderer.positionCount = EdgeCollider2D.pointCount;
        LineRenderer.SetPositions(EdgeCollider2D.points.Select(p => (Vector3)p).ToArray());

        if (Application.isPlaying)
        {
            enabled = false;
        }
    }

    private void OnDestroy()
    {
        foreach(var building in Buildings)
        {
            Destroy(building);
        }
    }
}
