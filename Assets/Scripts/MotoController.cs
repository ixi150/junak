﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotoController : MonoBehaviour
{
    public AnimationCurve gyroSpeedCurve;

    public bool canBeUsed;
    private bool isAlive = true;
    private const float _initialSpeed = 1000f;
    public float CurrentMaxSpeed;

    public float RotationSpeed = 100;
    public WheelJoint2D wheelFront;
    public WheelJoint2D wheelBack;


    private Slider OilMeter;
    private float baseOil;
    private float currentOil = 10000;
    public float currentDistance = 0;

    public static MotoController motoController;

    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private Vector3 cameraStartPos;

    [SerializeField]
    private float cameraStartFov;

    [SerializeField]
    private float cameraLerpSpeed;

    private List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();

    [SerializeField]
    public GameObject playerGameObject;

    [SerializeField]
    private Sprite deathSprite;

    [SerializeField]
    private Sprite fallSprite;

    [SerializeField]
    [Range(0, 10)]
    private float oilWastePerSecond = 1;

    float DeviceTilt
    {
        get
        {
#if UNITY_EDITOR
            return Input.GetAxis("Horizontal") / 2;
#else
            return Input.acceleration.x;
#endif
        }
    }

    private void Awake()
    {
        motoController = this;
        rigidbodies = new List<Rigidbody2D>(GetComponentsInChildren<Rigidbody2D>());
        rigidbodies.Add(GetComponent<Rigidbody2D>());
        ChangePhysicsSimulated(false);
    }

    private void Start()
    {
        OilMeter = GameObject.FindGameObjectWithTag("OilMeter").GetComponent<Slider>();
        baseOil = currentOil;
        CurrentMaxSpeed = _initialSpeed;
        Input.gyro.enabled = true;
    }

    public void Reset()
    {
        ChangePhysicsSimulated(false);
        currentOil = 10000f;
        currentDistance = 0;
        transform.position = new Vector3(0f, -5.5f, 0f);
        canBeUsed = false;
    }

    public void EnableAfterReset()
    {
        ChangePhysicsSimulated(true);
        canBeUsed = true;
        isAlive = true;
    }

    private void Update()
    {
        if (canBeUsed == false) return;

        currentDistance++;
        /* legacy todo
            - oil--
            - distance++(display on UI)
            - accelerometer<Tilt()>
            - ground check
        */

        Tilt();
        Ride();
        OilUpdate();
        JumpUpdate();
    }

    void JumpUpdate()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
#endif
#if UNITY_ANDROID
            if (Input.touchCount != 0)
#endif
            {
                Jump();
            }
    }

    private void ChangePhysicsSimulated(bool newState)
    {
        for (int i = 0; i < rigidbodies.Count; i++)
        {
            rigidbodies[i].simulated = newState;
        }
    }

    private void OilUpdate()
    {
        if (currentOil-- > 0)
        {
            OilMeter.value = currentOil;
        }

    }

    private void Tilt()
    {
        /*
			- if on ground -> rotate/tilt
		*/
        var target = Quaternion.Euler(0, 0, DeviceTilt * RotationSpeed);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime);
    }

    private void Ride()
    {
        /*
			- constant ride forward if oil variable is > 0
			- speed of motor controlled by variable
			- slow down to 0 if oil variable is =< 0
		*/
        if (currentOil <= 0)
        {
            Stop();
            return;
        }
        var wheelRide = new JointMotor2D
        {
            motorSpeed = -CurrentMaxSpeed * gyroSpeedCurve.Evaluate(DeviceTilt),
            maxMotorTorque = wheelBack.motor.maxMotorTorque
        };

        currentOil -= oilWastePerSecond * Time.deltaTime;
        //wheelFront.motor = 
        wheelBack.motor = wheelRide;
    }

    private void Stop()
    {
        var wheelStop = new JointMotor2D { motorSpeed = 0, maxMotorTorque = wheelBack.motor.maxMotorTorque };
        wheelFront.motor = wheelBack.motor = wheelStop;
    }

    private bool IsGrounded()
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, 8f);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].tag == "Ground")
            {
                return true;
            }
        }
        return false;
    }

    private void Jump()
    {
        if (IsGrounded() && isAlive)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce);
        }
    }

    private void DropPlayer()
    {
        StartCoroutine(DeathSequence());
    }

    private IEnumerator DeathSequence()
    {
        isAlive = false;
        playerGameObject.transform.parent = null;
        playerGameObject.GetComponent<SpriteRenderer>().sprite = fallSprite;

        Vector3 fallPos = playerGameObject.transform.position;
        Vector3 upPos = fallPos;
        upPos.y = upPos.y + 3f;

        while (playerGameObject.transform.position != upPos)
        {
            playerGameObject.transform.position = Vector3.MoveTowards(playerGameObject.transform.position, upPos, 10f * Time.deltaTime);
            yield return null;
        }

        while (playerGameObject.transform.position != fallPos)
        {
            playerGameObject.transform.position = Vector3.MoveTowards(playerGameObject.transform.position, fallPos, 10f * Time.deltaTime);
            yield return null;
        }
        playerGameObject.GetComponent<SpriteRenderer>().sprite = deathSprite;

        MainMenuManager.GetMainMenuManager().ShowDeathScreen();
    }

    public static MotoController GetMotoController()
    {
        return motoController;
    }

    public void CollectPickable(Pickable pickable)
    {
        if (pickable.pickableType == PickableType.Pasztecik)
        {
            CurrentMaxSpeed += pickable.boostValue;
        }
        else if (pickable.pickableType == PickableType.Paprykarz)
        {
            jumpForce += pickable.boostValue;
        }
        else if (pickable.pickableType == PickableType.Oil)
        {
            currentOil += pickable.boostValue;

            if (currentOil > baseOil)
            {
                currentOil = baseOil;
            }
        }
    }

    public void PlayGame()
    {
        StartCoroutine(PlayGameSeq());
    }

    private IEnumerator PlayGameSeq()
    {
        Camera.main.GetComponent<CameraController>().isRunning = true;
        while (Camera.main.orthographicSize < cameraStartFov)
        {
            Camera.main.orthographicSize += 0.01f * cameraLerpSpeed;
            yield return new WaitForSeconds(0.01f);
        }
        canBeUsed = true;
        ChangePhysicsSimulated(true);
        MainMenuManager.GetMainMenuManager().EnablePlayMode();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isAlive) return;
        if (collision.contacts.Length >= 1)
        {
            for (int i = 0; i < collision.contacts.Length; i++)
            {
                if (collision.contacts[i].collider.tag == "Ground")
                {
                    DropPlayer();
                }
            }
        }
    }
}